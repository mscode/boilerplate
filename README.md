# Boilerplate

A simple boilerplate for building with Grunt.
- `grunt create` creates the initial files
- `grunt bundle` bundles the files in the src/lib and src/css folder and minifies them to dist.
- `grunt server` starts a server, opens a browser window and watches index.html, main.scss and main.js. When an edit is saved it will lint the main.js, generate the main.css, copy the html to dist and minify css and js (also to dist).

## Used libraries and CSS

Normalize.css (by necolas)
https://github.com/necolas/normalize.css

## To Do


## Build

- run npm install
- run grunt create
- run grunt server
